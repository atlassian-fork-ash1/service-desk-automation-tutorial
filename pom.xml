<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>com.atlassian.plugins.tutorial.servicedesk</groupId>
    <artifactId>servicedesk-automation-extension</artifactId>
    <version>1.0.0-SNAPSHOT</version>

    <organization>
        <name>Atlassian</name>
        <url>https://www.atlassian.com/</url>
    </organization>

    <name>servicedesk-automation-extension</name>
    <description>Adds a new when, if and then rule component to JIRA Service Desk's automation feature.</description>
    <packaging>atlassian-plugin</packaging>

    <licenses>
        <license>
            <name>Apache License 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.jira</groupId>
            <artifactId>jira-api</artifactId>
            <version>${jira.version}</version>
            <scope>provided</scope>
        </dependency>
        <!-- Add dependency on jira-core if you want access to JIRA implementation classes as well as the sanctioned API. -->
        <!-- This is not normally recommended, but may be required eg when migrating a plugin originally developed against JIRA 4.x -->
        <!--
        <dependency>
            <groupId>com.atlassian.jira</groupId>
            <artifactId>jira-core</artifactId>
            <version>${jira.version}</version>
            <scope>provided</scope>
        </dependency>
        -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.10</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-annotation</artifactId>
            <version>${atlassian.spring.scanner.version}</version>
            <scope>provided</scope>
        </dependency>

        <!-- WIRED TEST RUNNER DEPENDENCIES -->
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi-testrunner</artifactId>
            <version>${plugin.testrunner.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>javax.ws.rs</groupId>
            <artifactId>jsr311-api</artifactId>
            <version>1.1.1</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.2.2-atlassian-1</version>
        </dependency>

        <!-- Uncomment to use TestKit in your project. Details at https://bitbucket.org/atlassian/jira-testkit -->
        <!-- You can read more about TestKit at https://developer.atlassian.com/display/JIRADEV/Plugin+Tutorial+-+Smarter+integration+testing+with+TestKit -->
        <!--
        <dependency>
            <groupId>com.atlassian.jira.tests</groupId>
            <artifactId>jira-testkit-client</artifactId>
            <version>${testkit.version}</version>
            <scope>test</scope>
        </dependency>
        -->
        <dependency>
            <groupId>com.atlassian.servicedesk</groupId>
            <artifactId>jira-servicedesk-automation-api</artifactId>
            <version>${servicedesk.automation.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.servicedesk</groupId>
            <artifactId>jira-servicedesk-automation-spi</artifactId>
            <version>${servicedesk.automation.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${springframework.version}</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>jira-maven-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <products>
                        <product>
                            <id>jira</id>
                            <instanceId>jira</instanceId>
                            <version>${jira.version}</version>
                            <applications>
                                <application>
                                    <applicationKey>jira-servicedesk</applicationKey>
                                    <version>${jira.servicedesk.application.version}</version>
                                </application>
                            </applications>
                            <pluginArtifacts>
                                <!-- Uncomment to install TestKit backdoor in JIRA. -->
                                <!--
                                    <pluginArtifact>
                                        <groupId>com.atlassian.jira.tests</groupId>
                                        <artifactId>jira-testkit-plugin</artifactId>
                                        <version>${testkit.version}</version>
                                    </pluginArtifact>
                                -->
                            </pluginArtifacts>
                        </product>
                    </products>
                    <systemProperties>
                        <atlassian.dev.mode>false</atlassian.dev.mode>
                    </systemProperties>
                    <skipITs>true</skipITs>
                    <instructions>
                        <Atlassian-Plugin-Key>${atlassian.plugin.key}</Atlassian-Plugin-Key>

                        <!-- Add package to export here -->
                        <Export-Package>
                            com.atlassian.plugins.tutorial.servicedesk.api,
                        </Export-Package>

                        <!-- Add package import here -->
                        <Import-Package>
                            com.atlassian.servicedesk.api.automation.*,
                            com.atlassian.servicedesk.spi.automation.*,
                            *
                        </Import-Package>

                        <!-- Ensure plugin is spring powered -->
                        <Spring-Context>*</Spring-Context>
                    </instructions>
                </configuration>
            </plugin>

            <plugin>
                <groupId>com.atlassian.plugin</groupId>
                <artifactId>atlassian-spring-scanner-maven-plugin</artifactId>
                <version>${atlassian.spring.scanner.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>atlassian-spring-scanner</goal>
                        </goals>
                        <phase>process-classes</phase>
                    </execution>
                </executions>
                <configuration>
                    <verbose>true</verbose>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <properties>
        <jira.version>8.0.0</jira.version>
        <jira.servicedesk.application.version>4.0.0</jira.servicedesk.application.version>
        <amps.version>8.0.0</amps.version>
        <plugin.testrunner.version>2.0.1</plugin.testrunner.version>
        <atlassian.spring.scanner.version>2.0.1</atlassian.spring.scanner.version>
        <!-- This property ensures consistency between the key in atlassian-plugin.xml and the OSGi bundle's key. -->
        <atlassian.plugin.key>${project.groupId}.${project.artifactId}</atlassian.plugin.key>
        <!-- TestKit version 6.x for JIRA 6.x -->
        <testkit.version>6.3.11</testkit.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <servicedesk.automation.version>4.0.0</servicedesk.automation.version>
        <springframework.version>5.1.6.RELEASE</springframework.version>
    </properties>

</project>
